<?php
require_once __DIR__ . "/vendor/autoload.php";
$filename = $_FILES["inputFile"]["tmp_name"];
$extension = pathinfo($_FILES["inputFile"]["name"], PATHINFO_EXTENSION);
$winnerNames = [];
$winnerTimes = [];

function getWinner($winners, $min,  $max) {
    do {
        $winner = rand($min, $max);
        if(!in_array($winner, $winners))
            return $winner;
        else
            continue;
    } while(false);
}

if($extension == "xlsx") {
    try {
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($filename);
        $worksheet = $spreadsheet->getActiveSheet();
        $columnTime = "A";
        $columnName = "C";
        $columnValue = "G";
        $columnValue2 = "J";

        $rowCount = 0; // řádek definice
        foreach ($worksheet->getRowIterator() as $row) {
            $rowCount++;
        }
        $winners = [];
        $winners[] = getWinner($winners, 2, $rowCount);
        $winners[] = getWinner($winners, 2, $rowCount);
        $winners[] = getWinner($winners, 2, $rowCount);

        $winnerNames = array_map(function ($winner) use ($worksheet, $columnName, $columnValue, $columnValue2) {
            $name = $worksheet->getCell($columnName . $winner)->getValue();
            $price = $worksheet->getCell($columnValue . $winner)->getValue();
            if($price == "")
                $price = $worksheet->getCell($columnValue2 . $winner)->getValue();
            return  "$name, $price Kč";
        }, $winners);

        $winnerTimes = array_map(function ($winner) use ($worksheet, $columnTime) {
            $timestamp = $worksheet->getCell($columnTime . $winner)->getValue();
            return PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($timestamp)->format("n.j.Y G:i:s");
        }, $winners);

        //    foreach ($winners as $winner)
        //        echo $worksheet->getCell($columnName . $winner)->getValue();

    } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
    } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
    }
}
else if($extension == 'csv') {
    $columnTime = 0;
    $columnName = 2;
    $columnValue = 6;

    $content = file_get_contents($filename);
    $lines = explode("\n", $content);
    $lineCount = count($lines);
    $winners = [];
    $winners[] = getWinner($winners, 1, $lineCount);
    $winners[] = getWinner($winners, 1, $lineCount);
    $winners[] = getWinner($winners, 1, $lineCount);
    $winnerNames = array_map(function ($winner) use($lines, $columnName, $columnValue) {
        return explode(";", $lines[$winner])[$columnName] .
            ', ' . explode(";", $lines[$winner])[$columnValue];
    }, $winners);
    $winnerTimes = array_map(function ($winner) use($lines, $columnTime) {
        return explode(";", $lines[$winner])[$columnTime];
    }, $winners);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Losování vítězů</title>
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <style>
        @font-face {
            font-family: Veneer;
            src: url(font/Veneer-W01-Two.ttf);
        }

        html {
            height: 100%;
        }

        body {
            margin: 25px;
            /*border: 1px solid black;*/
            background: url(img/center-bg.jpg) no-repeat transparent;
            background-size: cover;
        }

        #logo {
            position: absolute;
            left: 50px;
            top: 50px;
        }

        .vouchers {
            display: flex;
            flex-direction: column;
            width: 100%;
            margin-top: 190px;
        }

        .vouchers .row {
            display: flex;
            width: 100%;
            justify-content: space-around;
        }

        .voucher {
            text-align: center;
            position: relative;
        }

        .voucher::after {
            content: url("img/shadow.png");
            position: absolute;
            bottom: -50px;
            left: -80px;
            z-index: 1;
        }

        .voucher p {
            display: none;
            margin: 0;
            position: absolute;
            bottom: -30px;
            left: -50%;
            width: 200%;
            font-family: 'Oswald', sans-serif;
            font-display: swap;
            font-size: 16px;
            z-index: 2;
        }

        .voucher p.top {
            top: -25px;
        }

        .voucher-img {
            width: 250px;
        }

        .voucher.first {
            align-self: center;
        }

        .drawButton {
            width: fit-content;
            align-self: center;
            margin-top: 50px;
            padding: 20px 50px;
            border-radius: 25px;
        }

        .hiddenButton {
            position: absolute;
            right: 0;
            bottom: 0;
            width: 100px;
            height: 100px;
            color: transparent;
            background: transparent;
            border: transparent;
        }

        .hiddenButton:focus {
            outline: none;
            border:transparent;
            /*border-radius: 8px;*/
            /*border: 1px solid #0054a6;*/
            /*box-shadow: 0 0 5px  #0054a6;*/
        }
    </style>
</head>
<body>
<a href="index.php">
    <img id="logo" src="img/bushman-logo.png" />
</a>

<div class="vouchers">
    <div class="voucher first">
        <p id="winner2" class="top"><?= $winnerNames[2] ?></p>
        <img class="voucher-img" src="img/100.jpg">
        <p id="winner-time2"><?= $winnerTimes[2] ?></p>
    </div>

    <div class="row">
        <div class="voucher second">
            <p id="winner1" class="top"><?= $winnerNames[1] ?></p>
            <img class="voucher-img" src="img/50.jpg">
            <p id="winner-time1"><?= $winnerTimes[1] ?></p>
        </div>

        <div class="voucher third">
            <p id="winner0" class="top"><?= $winnerNames[0] ?></p>
            <img class="voucher-img" src="img/30.jpg">
            <p id="winner-time0"><?= $winnerTimes[0] ?></p>
        </div>
    </div>

    <img id="drawButton" class="drawButton" src="img/button.png" onclick="onButtonClick()" />
<!--    <button id="drawButton" class="drawButton" onclick="onButtonClick()">-->
<!--        -->
<!--    </button>-->
</div>

<button class="hiddenButton" onclick="onHidden()">Hidden button</button>

<script>
    var i = 0;
    var clicked = false;
    function onButtonClick() {
        if(i < 3) {
            setTimeout(function() {
                if(i < 3) {
                    var name = "winner" + i;
                    document.getElementById(name).style.display = "block";
                    i++;
                    if (i === 3) {
                        document.getElementById("drawButton").style.display = "none";
                    }
                }
            }, 5000);
        }
    }
    function onHidden() {
        var display = clicked ? "none" : "block";
        clicked = !clicked;
        document.getElementById("winner-time0").style.display = display;
        document.getElementById("winner-time1").style.display = display;
        document.getElementById("winner-time2").style.display = display;
    }
</script>
</body>
</html>